package nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String celsiusInput) {
        Double celsius = Double.parseDouble(celsiusInput);
        Double fahr = (celsius * 9.0/5.0) + 32.0;
        return fahr;
    }

}
